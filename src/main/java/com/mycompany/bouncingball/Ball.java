package com.mycompany.bouncingball;

import java.awt.Color;
import java.awt.Graphics;

public class Ball implements Form {

    private int radius;

    private int ball_speed_x;
    private int ball_speed_y;

    private int ballX;
    private int ballY;

    private Color c;

    public Ball(int radius, int ball_speed_x, int ball_speed_y, int ballX, int ballY, Color c) {
        this.radius = radius;
        this.ball_speed_x = ball_speed_x;
        this.ball_speed_y = ball_speed_y;
        this.ballX = ballX;
        this.ballY = ballY;
        this.c = c;
    }

    @Override 
    public void move(int box_width, int box_height) {
        ballX += ball_speed_x;
        ballY += ball_speed_y;

        if (ballX + radius > box_width) {
            ball_speed_x = -ball_speed_x;
            ballX = box_width - radius;

        } else if (ballX <= 0) {
            ball_speed_x = -ball_speed_x;
            ballX =0;
        } else if (ballY <= 0) {
            ball_speed_y = -ball_speed_y;
            ballY = 0;

        } else if (ballY + radius > box_height) {
            ball_speed_y = -ball_speed_y;
            ballY = box_height - radius;
        }
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(c);
        g.fillOval(ballX, ballY, radius, radius);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getBall_speed_x() {
        return ball_speed_x;
    }

    public void setBall_speed_x(int ball_speed_x) {
        this.ball_speed_x = ball_speed_x;
    }

    public int getBall_speed_y() {
        return ball_speed_y;
    }

    public void setBall_speed_y(int ball_speed_y) {
        this.ball_speed_y = ball_speed_y;
    }

    public int getBallX() {
        return ballX;
    }

    public void setBallX(int ballX) {
        this.ballX = ballX;
    }

    public int getBallY() {
        return ballY;
    }

    public void setBallY(int ballY) {
        this.ballY = ballY;
    }

    public Color getC() {
        return c;
    }

    public void setC(Color c) {
        this.c = c;
    }

}
