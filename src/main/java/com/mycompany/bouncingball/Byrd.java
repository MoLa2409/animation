package com.mycompany.bouncingball;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Byrd implements Form {

    private int seiteA;
    private int seiteB;

    private int bird_speed_x;
    private int bird_speed_y;

    private int birdX;
    private int birdY;

    private Image img;

    public Byrd(int sA, int sB, int bird_speed_x, int bird_speed_y, int birdX, int birdY) {
        this.seiteA = sA;
        this.seiteB = sB;
        this.bird_speed_x = bird_speed_x;
        this.bird_speed_y = bird_speed_y;
        this.birdX = birdX;
        this.birdY = birdY;

        try {
            img = ImageIO.read(new File("byrd.jpg"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void move(int box_width, int box_height) {
        birdX += bird_speed_x;
        birdY += bird_speed_y;

        if (birdX + seiteA > box_width) {
            bird_speed_x = -bird_speed_x;
            birdX = box_width - seiteA;

        } else if (birdX <= 0) {
            bird_speed_x = -bird_speed_x;
            birdX = 0;
        } else if (birdY <= 0) {
            bird_speed_y = -bird_speed_y;
            birdY = 0;

        } else if (birdY + seiteB > box_height) {
            bird_speed_y = -bird_speed_y;
            birdY = box_height - seiteB;
        }
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(img, birdX, birdY, seiteA, seiteB, null);
    }

    public int getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(int seiteA) {
        this.seiteA = seiteA;
    }

    public int getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(int seiteB) {
        this.seiteB = seiteB;
    }

    public int getBird_speed_x() {
        return bird_speed_x;
    }

    public void setBird_speed_x(int bird_speed_x) {
        this.bird_speed_x = bird_speed_x;
    }

    public int getBird_speed_y() {
        return bird_speed_y;
    }

    public void setBird_speed_y(int bird_speed_y) {
        this.bird_speed_y = bird_speed_y;
    }

    public int getBirdX() {
        return birdX;
    }

    public void setBirdX(int birdX) {
        this.birdX = birdX;
    }

    public int getBirdY() {
        return birdY;
    }

    public void setBirdY(int birdY) {
        this.birdY = birdY;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

}
