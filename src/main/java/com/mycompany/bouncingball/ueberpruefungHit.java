package com.mycompany.bouncingball;

public class ueberpruefungHit {

    public boolean hitRechteck(Bullets b, Rechteck r) {
        for (int j = 0; j < r.getSeiteB(); j++) {
            for (int i = 0; i < r.getSeiteA(); i++) {
                if (b.getX() == r.getRechteckX() + i && b.getY() == r.getRechteckY() + j) {
                    return true;
                }
            }
        }
        return false;

    }

    public boolean hitBall(Bullets b, Ball ball) {
        for (int j = 0; j < ball.getRadius(); j++) {
            for (int i = 0; i < ball.getRadius(); i++) {
                if (b.getX() == ball.getBallX() + i && b.getY() == ball.getBallY() + j) {
                    return true;
                }
            }
        }
        return false;

    }

    public boolean hitByrd(Bullets b, Byrd byrd) {
        for (int i = 0; i < byrd.getSeiteA(); i++) {
            for (int j = 0; j < byrd.getSeiteB(); j++) {
                if (b.getX() == byrd.getBirdX() + i && b.getY() == byrd.getBirdY() + j) {
                    return true;
                }
            }
        }
        return false;
    }
}
