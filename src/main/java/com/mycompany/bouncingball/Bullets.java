package com.mycompany.bouncingball;

import java.awt.Color;
import java.awt.Graphics;

public class Bullets implements Form {

    private int seiteA;
    private int seiteB;

    private int bullet_speed_y;
    private int bulletX;
    private int bulletY;

    private Color c;

    public Bullets(int seiteA, int seiteB, int rechteck_speed_y, int rechteckX, int rechteckY, Color c) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.bullet_speed_y = rechteck_speed_y;
        this.bulletX = rechteckX;
        this.bulletY = rechteckY;
        this.c = c;
    }

    @Override
    public void move(int box_width, int box_height) {
        bulletY = bulletY - bullet_speed_y;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(c);
        g.fillRect(bulletX, bulletY, seiteA, seiteB);
    }

    public int getX() {
        return bulletX;
    }

    public int getY() {
        return bulletY;
    }

}
