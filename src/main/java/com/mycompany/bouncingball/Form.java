package com.mycompany.bouncingball;

import java.awt.Graphics;


public interface Form {
    
    public void move(int box_width, int box_height);

    public void draw(Graphics g);
    
    
}
