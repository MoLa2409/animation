package com.mycompany.bouncingball;

import java.awt.Color;
import java.awt.Graphics;

public class Rechteck implements Form {

    private int seiteA;
    private int seiteB;

    private int rechteck_speed_x;
    private int rechteck_speed_y;

    private int rechteckX;
    private int rechteckY;

    private Color c;

    public Rechteck(int seiteA, int seiteB, int rechteck_speed_x, int rechteck_speed_y, int rechteckX, int rechteckY, Color c) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.rechteck_speed_x = rechteck_speed_x;
        this.rechteck_speed_y = rechteck_speed_y;
        this.rechteckX = rechteckX;
        this.rechteckY = rechteckY;
        this.c = c;
    }

    @Override
    public void move(int box_width, int box_height) {
        rechteckX += rechteck_speed_x;
        rechteckY += rechteck_speed_y;

        if (rechteckX + seiteA > box_width) {
            rechteck_speed_x = -rechteck_speed_x;
            rechteckX = box_width - seiteA;

        } else if (rechteckX <= 0) {
            rechteck_speed_x = -rechteck_speed_x;
            rechteckX = 0;
        } else if (rechteckY <= 0) {
            rechteck_speed_y = -rechteck_speed_y;
            rechteckY = 0;

        } else if (rechteckY + seiteB > box_height) {
            rechteck_speed_y = -rechteck_speed_y;
            rechteckY = box_height - seiteB;
        }
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(c);
        g.fillRect(rechteckX, rechteckY, seiteA, seiteB);
        
    }

    public int getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(int seiteA) {
        this.seiteA = seiteA;
    }

    public int getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(int seiteB) {
        this.seiteB = seiteB;
    }

    public int getRechteck_speed_x() {
        return rechteck_speed_x;
    }

    public void setRechteck_speed_x(int rechteck_speed_x) {
        this.rechteck_speed_x = rechteck_speed_x;
    }

    public int getRechteck_speed_y() {
        return rechteck_speed_y;
    }

    public void setRechteck_speed_y(int rechteck_speed_y) {
        this.rechteck_speed_y = rechteck_speed_y;
    }

    public int getRechteckX() {
        return rechteckX;
    }

    public void setRechteckX(int rechteckX) {
        this.rechteckX = rechteckX;
    }

    public int getRechteckY() {
        return rechteckY;
    }

    public void setRechteckY(int rechteckY) {
        this.rechteckY = rechteckY;
    }

    public Color getC() {
        return c;
    }

    public void setC(Color c) {
        this.c = c;
    }

}
