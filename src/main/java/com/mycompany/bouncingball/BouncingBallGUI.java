package com.mycompany.bouncingball;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

public class BouncingBallGUI extends JPanel implements KeyListener {

// Höhe und Breite der Box
    private int box_height = 410;
    private int box_width = 728;

    private int updateRate = 10;
    private Image bg;

    private ArrayList<Form> form;

    MoveImg moveimg = new MoveImg(50, 50, 50, 50);

    ueberpruefungHit ueberpruefung_hit = new ueberpruefungHit();

    public BouncingBallGUI(Image i) {

        bg = i;

        form = new ArrayList<>();

        Ball ball_1 = new Ball(60, 1, 3, 20, 20, Color.green);
        form.add(ball_1);

        Ball ball_2 = new Ball(75, 3, 1, 20, 20, Color.cyan);
        form.add(ball_2);

        Rechteck rechteck_1 = new Rechteck(25, 75, 2, 3, 150, 150, Color.blue);
        form.add(rechteck_1);

        Byrd bird_1 = new Byrd(50, 50, 5, 3, 20, 75);
        form.add(bird_1);

        this.setPreferredSize(new Dimension(box_width, box_height));

// Erstellen einen neuen Thread in dem die Methode startBall ausgeführt wird
        Thread th = new Thread(new Runnable() {

            @Override
            public void run() {
                startBall();

            }
        });
        th.start();

    }

    private void startBall() {
        while (true) {
            ArrayList<Bullets> bullets_to_remove = new ArrayList<>();

// In dieser for-Schleife werden alle Ball Objekte, die sich in der Array-List befinden 1-Mal bewegt
            for (int i = 0; i < form.size(); i++) {
                Form f = form.get(i);
                if (f instanceof Bullets) {
                    Bullets b = (Bullets) f;
                    if (b.getY() < -10) {
                        bullets_to_remove.add(b);
                    }

                }

                f.move(box_width, box_height);

            }

            form.removeAll(bullets_to_remove);

// Überprüfe ob sich ein Bullet mit einem Objekt überschneidet, wenn ja entferne beide
            ArrayList<Form> form_to_remove = new ArrayList<>();

            for (int i = 0; i < form.size(); i++) {
                Form f_1 = form.get(i);
                if (f_1 instanceof Bullets) {
                    Bullets b = (Bullets) f_1;
                    for (int j = 0; j < form.size(); j++) {
                        Form f_2 = form.get(j);

                        if (f_2 instanceof Rechteck) {
                            Rechteck r = (Rechteck) f_2;
                            if (ueberpruefung_hit.hitRechteck(b, r)) {
                                form_to_remove.add(f_2);
                                form_to_remove.add(f_1);
                            }

                        } else if (f_2 instanceof Ball) {
                            Ball ball = (Ball) f_2;
                            if (ueberpruefung_hit.hitBall(b, ball)) {
                                form_to_remove.add(f_2);
                                form_to_remove.add(f_1);
                            }
                        } else if (f_2 instanceof Byrd) {
                            Byrd byrd = (Byrd) f_2;
                            if (ueberpruefung_hit.hitByrd(b, byrd)) {
                                form_to_remove.add(f_2);
                                form_to_remove.add(f_2);
                            }
                        }

                    }
                }

            }

            form.removeAll(form_to_remove);

            try {
                Thread.sleep(updateRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.repaint();
        }

    }

// Wir überschreiben die Methode paintComponent um unsere Grafiken zu zeichnen
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        g.drawImage(bg, 0, 0, null);

        for (int i = 0; i < form.size(); i++) {
            Form f = form.get(i);
            f.draw(g);

        }
        moveimg.draw(g);

        g.dispose();

    }

    public static void main(String[] args) {

        try {

            ImageObserver comp = new JComponent() {
                private static final long serialVersionUID = 1L;
            };

            Image img = ImageIO.read(new File("bg.jpg"));

            ImageIcon icon = new ImageIcon("sus.png");

            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(img.getWidth(null) + 15, img.getHeight(null) + 38);
            frame.setResizable(false);
            frame.setTitle("BIG BALLS");
            frame.setIconImage(icon.getImage());

            frame.setLocationRelativeTo(null);

            BouncingBallGUI gui = new BouncingBallGUI(img);
            frame.setContentPane(gui);

            frame.addKeyListener(gui);

            frame.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(BouncingBallGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_S || e.getKeyCode() == KeyEvent.VK_D || e.getKeyCode() == KeyEvent.VK_W) {
            moveimg.action(box_width, box_height, e);
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            Bullets b = new Bullets(5, 20, 4, moveimg.getMoveImgX() + moveimg.getSeiteA() / 2, moveimg.getMoveImgY(), Color.yellow);
            form.add(b);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
