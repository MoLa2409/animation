package com.mycompany.bouncingball;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class MoveImg {

    private int seiteA;
    private int seiteB;

    private int MoveImgX;
    private int MoveImgY;

    private Image img;

    public MoveImg(int sA, int sB, int MoveImgX, int MoveImgY) {
        this.seiteA = sA;
        this.seiteB = sB;

        this.MoveImgX = MoveImgX;
        this.MoveImgY = MoveImgY;

        try {
            img = ImageIO.read(new File("sponge.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void action(int box_width, int box_height, KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_A) {
            if (MoveImgX + seiteA < 0) {
                MoveImgX = 728;
            }
            MoveImgX -= 10;
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
            if (MoveImgX > 728) {
                MoveImgX = 0 - seiteA;
            }
            MoveImgX += 10;
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
            if (MoveImgY > 410) {
                MoveImgY = 0 - seiteB;
            }
            MoveImgY += 10;
        } else if (e.getKeyCode() == KeyEvent.VK_W) {
            if (MoveImgY + seiteB < 0) {
                MoveImgY = 410;
            }
            MoveImgY -= 10;
        } 
    }

    public void draw(Graphics g) {
        g.drawImage(img, MoveImgX, MoveImgY, seiteA, seiteB, null);
    }

    public int getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(int seiteA) {
        this.seiteA = seiteA;
    }

    public int getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(int seiteB) {
        this.seiteB = seiteB;
    }

    public int getMoveImgX() {
        return MoveImgX;
    }

    public void setMoveImgX(int MoveImgX) {
        this.MoveImgX = MoveImgX;
    }

    public int getMoveImgY() {
        return MoveImgY;
    }

    public void setMoveImgY(int MoveImgY) {
        this.MoveImgY = MoveImgY;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }
    

}
